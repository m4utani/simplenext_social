## Run Project

In the project directory, you can run:

 `docker compose up --build `

## Login page
Open [http://localhost:3000/login](http://localhost:3000/login) to view it in the browser.
```
{
    "email":"admin@g.com",
    "password":"123"
}
```